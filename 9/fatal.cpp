/*Tür dönüştürme operatörlerinin yanlş kullanımları*/

enum Color {white, Red, Green};
enum class Color2 {white, Red, Green};


int main(int argc, char const *argv[])
{
    int x = 10;
    double dval = 2.8;

    const int y = 10;
    auto yy = (&y);     //const int *adress

    const int* ptr = nullptr;

    x = reinterpret_cast<int>(dval); //invalid type conversionC/C++(171)
    x = static_cast<int>(dval);      //Doğrusu

    
    
    reinterpret_cast<int*>(ptr);    //reinterpret_cast cannot cast away const or other type qualifiersC/C++(694)
    const_cast<int*>(ptr);          //Doğrusu


    /*tür dönüştürme operatorleri ile 2 tür dönüşümü birden yapmak*/
    char* p = reinterpret_cast<char*>(yy);
    char* p2 = const_cast<char*>(yy);
    char* p3 = reinterpret_cast<char*>(const_cast<int *>(yy)); // ya böyle
    char* p4 = const_cast<char*>(reinterpret_cast<const char*>(yy)); //yada böyle



    Color mycolor{ Red };
    int ival = mycolor; //Bu zaten legal. Ama tersi legal değil
    mycolor = ival;
    mycolor = static_cast<Color>(ival); //legal

    /*enum class dan int e*/
    Color2 mycolor2{ Color2::Red };
    int ival2 = mycolor2;  //enum da legal fakat enum class ta tamsayı dönüşümü syntax hatası
    int ival3 = static_cast<int>(mycolor2);


    return 0;
}
