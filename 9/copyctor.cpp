#include    <iostream>
/*
C++ 

*/
class Nec {
public:
    Nec(int val = 0) : mx{val}                              //Default Constructor
    {
        std::cout << "Nec default ctor - " << this << "\n";
    }    
    ~Nec()                                                  //Default Destructor
    {
        std::cout << "Nec destructor - " << this << "\n";
    }
    Nec(const Nec &other) : mx(other.mx)                    //copy constructor : görebilmek için yazdık burada yazmamızı gerektirecek bi esnaryo yok.
    {    
        std::cout << "Nec copy constructor -  " << this << "   &other :  " << &other << "\n";
    }
private:
    int mx;
};



int main(int argc, char const *argv[])
{
    Nec y = 10;
    Nec x = y;      //burada copyctor y değişkenini hayata getirmek için çalıştırılacak. 
    /*y nesnesi kopyalama yolu ile oluşturulduğu için  copy constructor çağırıldı.*/
    
    std::cout << "&x : " << &x << "\n";
    std::cout << "&y : " << &y << "\n";


    return 0;
}
