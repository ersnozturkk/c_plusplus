/*
Nesnemiz fiziksel const olmak zorunda değil. constu 2'ye ayırabiliriz.

fiziksel constness (değişkenin tanımında const anahtar sözcüğünün kullanılması)
contractual constness (söze dayalı const - mesela fonksiyonların değişkeni değiştirmeyeceğine söz vermesi)
*/


void func(const int* ptr)
{
    const int x = 10;           //Değiştirilmesi her zaman tanımsız davranış. Bu fiziksel const
    /*
    *ptr ifadesi const mu? Evet derleyiciye göre const. Ben *ptr yi değiştirmeyeceğim diyoruz.
    *ptr sözleşmeye bağlı const.
    Soru: *ptr aynı zamanda fiziksel olarak const mu? cevap: it depend (Örnek main'de)
    */
}


int main(int argc, char const *argv[])
{
    int x = 10;
    func(&x);       /*Mesela burda x in adresini func'a gönderdiğimizde *ptr sözleşmeye bağlı const. ,
    *ptr ye atama yapmaya çalışırsak sentaks hatası fakat fiziksel olarak *ptr const değil.*/

    
    return 0;
}
