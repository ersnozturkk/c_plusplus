#include <iostream>

char* Strchr(const char* p, int ch)
{
    while (*p)
    {
        if(*p == ch)
            return (char*)p;    // Burada const-cast var. 
        ++p;
    }
    if(ch == '\0')
        return (char*)p;        // Burada const-cast var. 

    return NULL;    
}


int main(int argc, char const *argv[])
{
    char a[] = "ersinozturl";
    Strchr(a,'e');
    /*a yı fonksiyona gönderdiğimizde bir const-cast hatası oluşmaz. çünkü a fiziksel const değil*/
    return 0;
}
