#include <iostream>
/*
otomatik ömürlü bir sınıf nesnesi default init. edildiğinde sınıfın default construstor'u varsa default constructor çağırılır ve sınıf değişkenleri default init. edilir. =>     Myclass a;
Sınıfın default constructorunu kendimiz tanımlar isek veri elemanları default initialize tarafından zero initialize edilmediği için value init. yapılsa bile değerler 
çöp değer olur.  =>     Myclass a{};

*/

class Myclass{
public:    
    Myclass(){}         //kaldır bak farkı gör
    void print()const
    {
        std::cout << x << " - " << y << " - " << z << '\n';
    }
private:
    int x, y, z;

};



int main(int argc, char const *argv[])
{
    Myclass a;
    Myclass b{};

    a.print();
    b.print();
    return 0;
}
