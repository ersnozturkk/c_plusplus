#include <iostream>

class Nec
{
private:
        int mx;
public:
        Nec(int val = 0) : mx{val}
        {       
        std::cout << "Nec default const this = " << this << "\n";
        }

        Nec(const Nec& other) : mx(other.mx)
        {
        std::cout << "Copy ctor    this = " << this << "   &Other :" << &other << "\n";
        }

        ~Nec()
        {
        std::cout << "Nec destructor this = " << this << "\n";
        }


};

 
void func(Nec a)
{
        std::cout << "Func cagirildi\n";
        std::cout << "Func sona erdi\n";
}

int main(int argc, char const *argv[])
{
    Nec x(10);
    Nec y = x;
    return 0;
}

