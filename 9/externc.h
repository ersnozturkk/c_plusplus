extern "C" int externcexample();



extern "C" {
    int f1(void);
    int f2(void);
    int f3(void);
    int f4(void); 
}


/*C derleyicisinin atladığı c++ derleyicisinin atlamadığı extern C yapısı - Önerilen kullanım*/

#ifdef __cplusplus          /*Buraya C++ derleyicisi girecek. C derleyicileri girmeyecek*/
extern "C" {                /*Çünkü __cplusplus makrosu C++'da tanımlı sadece*/
#endif
    int f5(void);
    int f6(void);
    int f7(void);
    int f8(void); 

#ifdef __cplusplus
}
#endif