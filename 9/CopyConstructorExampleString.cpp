/*
Bu kod güncellemeli gidecek. Bağlantıları kopyalanacak. 1 2 3 4 diye devam edecek.

*/
/*
bu örnekte sx foo fonksiyonuna gönderilince default const çalışıyor ve &mx &s ye kopyalanıyor.
Fonksiyondan çıkarken de destructor çalıştığı için fonksiyon çıkışında adres koplayamadan dolayı 
sx i kaybediyoruz Bunu önlemek için 2 ye bak. 
*/
#include "sentence.h"

void foo(Sentence s)
{
    std::cout << "foo işlevi cagirildi.\n";
    s.print();
    std::cout << "foo işlevi sona erdi.\n";
    
}


int main(int argc, char const *argv[])
{
    Sentence sx{"Sabah kalkar kalkmaz video izledim."};
    sx.print();
    auto len = sx.lenght();

    foo(sx);
    getchar();
    sx.print();
    return 0;

}


Sentence::Sentence(const char*p) : mlen{strlen(p)}
{
    mp = static_cast<char*>(malloc(mlen+1));

    if (!mp) {
        std::cout << "bellek yetersiz\n";
        exit(EXIT_FAILURE);
    }
    std::cout << "edinilen bellek bloğunun adresi : " << static_cast<void*>(mp) << "\n";
    strcpy(mp,p);
}

Sentence::~Sentence() 
{
        std::cout << static_cast<void*>(mp) << " adresindeki blok free ediliyor." << "\n";
        free(mp);

}

void Sentence::print()const
{
    std::cout << "(" << mp << ")" << "\n";
}

int Sentence::lenght()
{
    return mlen;
}

