

class MyClass{

public:
    bool operator!();
    bool operator!(MyClass); //too many parameters for this operator functionC/C++(344)
    bool operator<(MyClass); // Kural: Burada *this sol operant oluyor. Bu yüzden de tek parametre almak zorunda
    bool operator>();       // too few parameters for this operator functionC/C++(345)
    bool operator=(MyClass,MyClass);      // too many parameters for this operator functionC/C++(344)

//NOT: (+,-,*,&) tokenları hem unary hem binary operant olarak kullanılıyor.
    MyClass operator+()const;   // overload edilen : sign(işaret operatoru)
    MyClass operator+(MyClass)const; //overload edilen : toplama operatoru

    MyClass& operator*()const;   // overload edilen : içerik operatoru
    MyClass& operator*(MyClass)const;   // overload edilen : Çarpma operatoru

    MyClass& operator&()const;   // overload edilen : Adres operatoru
    MyClass& operator&(MyClass)const;   // overload edilen : bitwise operatoru

    /*
        ++x : prefix 
        x++ : postfix operatorlerinin ayrı ayrı overload edebiliyoruz.
    */


};

bool operator!(MyClass);
bool operator!(MyClass,MyClass); //too many parameters for this operator functionC/C++(344) 
bool operator~(); //too few parameters for this operator functionC/C++(345)
bool operator>(MyClass,MyClass);
bool operator<(MyClass); //too few parameters for this operator functionC/C++(345)
bool operator=(MyClass,MyClass); //'operator=' must be a member functionC/C++(341)

