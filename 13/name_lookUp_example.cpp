#include <iostream>

class MyClass {

public:
    static int foo()
    {
        return 1;
    }    
    static int func(int x)
    {
        return 1;
    }

    static int x;
    static int y;
    static int z;
};



int foo()
{
    return 2;
}


int MyClass::x = foo();
int MyClass::y = ::foo();
int MyClass::z = func(); //isim arama bitti ve class scope da buulundu.  Burada hata argüman göndermememiz. Yani namespace deki foo() overload değil. Önemli!!!
//too few arguments in function callC/C++(165)


int main(int argc, char const *argv[])
{
    std::cout << MyClass::x <<"\n"; //Sınıfın üye elemanıona ilk değer veren fonksiyon önce class scope da aranır. O yüzden 1 gelir.
    std::cout << MyClass::y <<"\n"; //unary Çözünürlük operatoru fonksiyonu namespace de aranmasını sağlayor.

    return 0;
}
