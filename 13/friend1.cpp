



class MyClass {
    public:
        friend int gfunc(); //gfunc friend olarak bildirildi
    private:
        int mx;
        void foo();
};


int gfunc() {
    MyClass m;;
    m.mx = 12;
    m.foo();
}

int gfoo() {    //bu fonksiyonun class içinde friend bildirimi olmadıgı için sentaks hatası
    MyClass m;;
    m.mx = 12;
    m.foo();
}


int main(int argc, char const *argv[])
{
    MyClass m2;
    m2.mx = 34;
    return 0;
}
