#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

//fighter.h
class Fighter {
public:
    Fighter(const std::string &name) : m_name{name} //contructor name özel üye elemanına ismi atadı.
    {
        m_svec.push_back(this);
    }

    ~Fighter();
    //...

    void ask_help();
    std::string Name()const
    {
        return m_name;
    }
private:
    std::string m_name;
    inline static std::vector<Fighter*> m_svec; // <????> Template arguman
};



//fighter.cpp
// std::vector<Fighter*> Fighter::m_svec; inline yaptığım için burada init yapmama gerek yok C++17


Fighter::~Fighter()
{
    auto iter = std::find(m_svec.begin(),m_svec.end(),this);

    if(iter != m_svec.end())
    {
        m_svec.erase(iter);
    }
}


void Fighter::ask_help()
{
    std::cout << "Ben savaşcı " << m_name << "dusmanlar oldurecek beni, yetisin!!!\n";
    for(auto p : m_svec)
    {
        if(p != this)
            std::cout << p->m_name << " \t";
    }
}
int main(int argc, char const *argv[])
{
    std::cout << "Hello\n";

    Fighter f1{"Yunus"};
    Fighter f2{"Selami"};

    auto pf1 = new Fighter("Ayse");
    auto pf2 = new Fighter("Zeliha");

    Fighter f3{"Muhittin"};

    delete pf1;     //Ayşe vefat etti. :)

    pf2->ask_help();

    return 0;
}
