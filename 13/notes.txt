Statik veri elemanları ve statik üye fonksiyonları devam...

static üye fonksiyonları sıınıfın private üyelerine erişebilir.
bu fonksiyonlarda this pointer ını kullanamayız.
sınıfın non-static üye fonksiyonlarını ve üye elemanlarını direk ismiyle kullanamayız.



KURGU:
    Sınıfın static bir veri elemanı varsa o değişkeni get eden bir fonksiyon olur. kurgu.cpp

KURGU:
    Mesela sınıfın static bir değiişkeni olsun. ve bu nesneden kaçtane oluşturulduğunu tutsun. Bu sayede kaç nesne olduğu sayılabilir.


******SINIF NESNELERİNİN BİRBİRLERİNE ERİŞİMLERİNİ SAĞLAMAK******

SAKIN YAPMA : bir başlık dosyasında aslı using namespace yazma!!! (using namespace std;) isim alanları kısmında göreceğiz.

NAMED CONSTRUCTOR IDIOM (isimlendirilmiş kurucu işlevi) 
    Client codların constructor a doğrudan çağrı yapması yerine isimlendirilmiş static bir üye fonksiyonu veriyouz.

Design Pattern ne demek? (Tasarım kalıbı - tasarım örüntüsü) - Aslında Object Oriented Design Pattern

    pattern : Öyle bir kalıpi birden fazla örnekte bu kalıbı kullanabiliyoruz. Dilden bağımsız

    idiom : Dile bağlı (C++ idioms)


============================================================================================
Singlton pattern :Bir anti-pattern

Bir sınıf türünden sadece tek bir nesne oluşacak ve bütün Client lar bu nesneyi kullanabilecek.
(global access)

Mayers Singlton


friend bildirimleri
    Öyle bi bildirim varki bu bildirim ile bir sınıf
        a) bir fonksiyonlarda
        b) bir başka sınıfa
        c) bir başka sınıfın üye fonksiyonuna erişim izni verebilir. 
        
    Konu bir sınıfın private bölümüne erişim yapmayı mümkün kılmakla ilgili.
    Bu mekanızma sınıfın kendi kodları için kullanılıyor.
    friend bildirimleri amacı dışında kullanma!!! C++ çalışma mantığını bozar.

    Ne için kullanırız ?
        a) bir global fonksiyon için yapılabilir.
        b) bir başka sınıfın üye fonksiyonu için yaılabilir.
        c)bir başka sınıf için yapılabilir. friend1.cpp

NOT: friend bildirimini sınıfını pubilc,private,produte bölümlerinden istediğimizde yapabiliriz.
NOT: Özel üye fonksiyonlarına da friendlik verebiliriz. 
NOT: friendlik verilen sınıfın complate type olması gerekiyor.

NOT: fried lik geçişken değil. Yani arkadaşımın arkadaşı benim arkadaşım olmak zorunda değil.



OPERATOR OVERLOADING (OPERATOR YÜKLEMESİ)

    Fonksiyon çağırmanın bir başka yolu aslında. Bir sınıf nesnesi bir operatorun operandı yapıldığında derleyici durumdan vazife çıkararakl
    derleyici ifadeyi bir fonksiyon çağrısına dönüştürüyor.
        a+b yaptığımızda bir fonksiyon çağırmış oluyoruz.
        Bu yöntem ile çağırılan fonksiyonlara operator functions deniyor.

ÖNEMLİ NOT: Bu fonksiyonlar static üye fonksiyonu olamaz!!!

neden ? ne işe yarayacak ? Çalışma zamanına ilave bi maliyet söz konsu mu?
    Hayır,bi maliyeti yok. Tamamen derleme zamanı ile alakalı.

    Operator Overloading : bir fonksiyonu ismi ve fonksiyon çağırma operatoruyle çağırmak yerine bir sınıf nesnesini bir operatorun operandı yapıyoruz. ve derleyici bunu bir fonksiyona çağrıya dönüştürüyor.

NOT: Burada amaç Client kodu yazan kişinin işini kolaylaştırmak.
=======================================OPERATOR OVERLOADING İŞİN ALFABESİ=====================================================
 1-) C++ dilinin legal operatorleri dışında ki hiç bir simgeyi operator olarak yökleyemeyiz. Örn : x @ y yapamayız.
 2-) Operator Overloading yaparken operandların en az birinin sınıf türünden olması gerekiyor.  
 3-) Bazı operatorler overload edilemiyor. - Çözünürlük operatorü(::) - Nokta operatoru (.)  - Koşul operatoru (? :) - Sizeof operatoru
 4-) Operator overloading mekanızmasında olan fonksiyonlar keyfi şekilde isimlendirilemiyor. 
        operator+ -> toplama operatorünü overload eden fonksiyonun ismi
        operator& -> & operatorünü overload eden fonksiyonun ismi
5-) Bazı fonksiyonlar global operator fonksiyonu ile overload edilemiyor. Yani bazı oeprator overload fonksiyonları global fonksiyon olamaz.,
Sadece bir sınıfın üye fonksiyonu olabilir. Bunlar;

    operator[]
    operator()
    type-cast operator functions
    operator->
    operator=

6-) Bu operator overload fonksiyonlar isimleri ilede çağırılabilir. Bazen ismiyle çağırmak daha okunur olabiliyor.
    Bazende ismiyle çağırmak gerekli olacak.

7-) (Biri hariç : Fonksiyon çağrı operatoru) operator fonksiyonları "default argument" alamıyor.

8-) Statik üye fonksiyonuı olamaz. Ya global fonksiyon olacak yada sınıfın üye fonksiyonu olacak.

9-) operator fonksiyonlarında operatorlerin arity sini değiştiremeyiz. atiry -> unery yada binary yada ternary
        unary olması : tek operant alması demek  !x , ~y
        binary olması : iki operant alması demek a < b , a*b Orn:operatoroverload.cpp

10-) operatorlerin öncelik seviyesini ve öncelik yönünü değiştiremeyiz. (priority - precedence)

    





