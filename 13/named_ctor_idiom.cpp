


class Myclass {
public:
    static Myclass* createObject()
    {
        return new Myclass;
    }
private:
    Myclass(); //ctor private
};



int main(int argc, char const *argv[])
{
    Myclass mx; //"Myclass::Myclass()" (declared at line 7) is inaccessibleC/C++(330) private olduğu için sentaks hatası

    auto mx2 = Myclass::createObject();

    return 0;
}
