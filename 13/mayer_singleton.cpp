

class Singleton {           //Standart Singleton
    public:
    static Singleton& get_instance()
    {
       static Singleton instance;

       return instance;
    }


    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;

    private:
        Singleton();
};



int main(int argc, char const *argv[])
{
    Singleton x;
    return 0;
}
