#include <iostream>

class Person{
public:
    Person()
    {
        ++live_object_count;
        ++total_object_count;
    }
    //n tane consructor olabilir. her bir contortor (Diğer kurucu işlevler de...)
    ~Person()
    {
        --live_object_count;
    }

private:
    inline static size_t live_object_count{};
    inline static size_t total_object_count{};

};
