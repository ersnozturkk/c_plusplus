

class MyClass{

private:
    MyClass();

};

class MyClass2{
public:
    MyClass2();

};

class MyClass3{
public:
    static MyClass3* createObject()
    {
        return new MyClass3();
    }

};

int main(int argc, char const *argv[])
{
    MyClass x;  //constrctor private oldugu için ulasılamaz.
    MyClass2 y; //ctor public
    return 0;
}
    