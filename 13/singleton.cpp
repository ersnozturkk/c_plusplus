

class Singleton {           //Standart Singleton
    public:
    static Singleton& get_instance()
    {
        if(!mp)
        {
            mp = new Singleton;
        }
        return *mp;
    }

    ~Singleton()
    {
        delete mp;
    }
    Singleton(const Singleton&) = delete;
    Singleton& operator=(const Singleton&) = delete;
    void fuunc();
    void foo();
    private:
        Singleton();
        inline static Singleton* mp{};
};



int main(int argc, char const *argv[])
{
    Singleton x;
    return 0;
}
