#include <iostream>
#include <new>
#include <string>


void* operator new(size_t n)            //New operatörünü overload ettik
{
    std::cout << "operator new called n = " << n << "\n";
    void* vp = std::malloc(n);
    if(!vp){
        throw std::bad_alloc{};
    }

    std::cout << "the address of the allocated block = " << vp << "\n";
    return vp;
}


int main(int argc, char const *argv[])
{
    std::string str{"Sonbaharın dusen son yapragi"}; //Bu yazı heap de tutuluyor.
    return 0;
}
