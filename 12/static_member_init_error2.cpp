#include <iostream>
#include <string>


using namespace std;


class Myclass{
        //static int x = 10;  // Geçersiz const değil
        const static int y = 10; //geçerli
        //const static double d = 1.0; //geçersiz tam sayı değil
        constexpr static int y2 = 55; // geçerli
        constexpr static double y2 = 55; // geçerli - double ı bu şekilde init edebiliriz.
        
};

int main(int argc, char const *argv[])
{

    return 0;
}
