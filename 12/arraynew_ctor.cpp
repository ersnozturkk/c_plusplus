#include <iostream>

class Myclass
{
private:
    char buffer[16]{};
public:
    Myclass()
    {
        std::cout << "ctor this : " << this << "\n";
    }
    ~Myclass()
    {
        std::cout << "d ctor this : " << this << "\n";
    }
};

int main(int argc, char const *argv[])
{
    auto p = new Myclass[4];  //Constructor 4 kere çağırılacak. (p nin türü : Myclass *p)

    //delete p; //Tanımsız davranış array new i böyle öldürme bunu yapma 
    delete[] p; //Array new i delete eden operator
    return 0;
}

