#include <iostream>
#include <stdio.h>
#include <utility>


using namespace std;


void func(int&& r)
{
    std::cout << "func(int&&)\n";
}

void func(const int& r)
{
    std::cout << "func(const int&)\n";
}




int main(int argc, char const *argv[])
{
    int a;
    const int b{};

    func(a); // L value ile çaşırdık.
        std::cout << "----------------------------------------------------------------------------------------------------------------------------\n";

    func(23); // R value exp ile çağırdık.
            std::cout << "----------------------------------------------------------------------------------------------------------------------------\n";
    func(b); //const bi nesne ile çağırdık.
            std::cout << "----------------------------------------------------------------------------------------------------------------------------\n";
    func(move(b));
            std::cout << "----------------------------------------------------------------------------------------------------------------------------\n";

    return 0;
}

