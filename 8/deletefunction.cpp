


void func(double);
void func(int) = delete;            //func fonksiyonunun bir tamsayı değişkeni ile çağırılmasını engelledik.


int main(int argc, char const *argv[])
{
    int ival{ 3 };
    func(ival);            //func fonksiyonunun bir tamsayı değişkeni ile çağırılmasını engelledik.
    return 0;
}



