#include <iostream>

class MyClasss {
public:
    MyClasss();
    void print()const;
private:
    int a, b;
    int& r;             
};


MyClasss::MyClasss() :r{a}   /*: a{3.8} ,b(4.5)*/   /* :r{a} */     { //constructor initialized list
//küme parantezi ile ilkdeğer verilmesi durumunda narrowing conversion geçersiz.
    std::cout << "Myclasss default ctor\n";
}

void MyClasss::print()const
{
    std::cout << "a = "  << a << " b = " << b << "\n";
}



int main(int argc, char const *argv[])
{
    MyClasss x;

    x.print();
    return 0;
}
