#include "date.h"
#include <iostream>

Date::Date(int d, int m, int y): md{ d + 9}, mm( m * 2 ), my{ y } {} //constructor kapsamını oluşturmak mecburi


void Date::print()const
{
    std::cout << md << " - " << mm << " - " << my << "\n";
}

int main(int argc, char const *argv[])
{
    Date mydate(15, 2,1999);
    Date mydate2{15, 2.3,1999};

    mydate.print();
    
    return 0;
}
