#include <iostream>

using namespace std;

void func(int*p){
    cout << "(int*p)\n";
}

void func(const int*p){
    cout << "(const int*p)\n";
}



int main(int argc, char const *argv[])
{
    const int x{10};
    int y{20};
    
    func(&x);    
    func(&y);

    return 0;
}