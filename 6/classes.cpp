//class members
    //data member(veri elemanları)
        //-non-static data members
        //-static data mebers
    //member function(Üye fonksiyon) (method)
    //member type - type member - nested type


struct Ers2     //bütün elemanları public olacak yapılar oluştururken kullanılıyor.
{
    int mx;
};


class Ers {
    //Bildirime açık bölge. burada class members bulunur.

    int mx;  //mx default olarak private data member

    public:
        int x;  // x sınıfın public data member ı
        void foo(int);  // public member function
    private:
        int y;      //private data member
        typedef int Word; //private type member
    protected:
        void f();       //protected member function
};

int main(int argc, char const *argv[])
{
    
    return 0;
}
