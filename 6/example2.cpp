#include <iostream>

int g(int);
int g(int);


void func(const int&)
{
    std::cout << "func(const int&)\n";
}

void func(int&)
{
    std::cout << "func(int&)\n";
}

void func(int&&)
{
    std::cout << "func(int&&)\n";
}


int main(int argc, char const *argv[])
{
    int x = 10;
    const int y = 209;

    func(x); 
    func(y);
    func(20);  //Dikkat burada ikiside vaiable fakat R value olana öncelik kabul edilir.

    return 0;
}
