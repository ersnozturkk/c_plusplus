#include <iostream>


/*
                                C                                           C++
++x                             R value                                     L value
--x                             R value                                     L value
(x, y)                          R                                           L
a > 5 ? x : y                   R                                           L
atama ifadeleri                 R                                           L



fonksiyonun geri dönüş değeri referans ise L value aritmetikse R value     
*/

int main(int argc, char const *argv[])
{
    int x = 10;

    //int& r = x++; //initial value of reference to non-const must be an lvalueC/C++(461)
    int& r = ++x; //geçerli. ++x L value expression
    return 0;
}
