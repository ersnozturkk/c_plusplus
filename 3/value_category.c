/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/


// 0 ->48 , 1->49
#include <stdio.h>
#include "stdlib.h"

#define OFFSET 48


char * addBinary(char * a, char * b){
    char* retval;
    int digitnumber = 0;
    int fark = 0;
    while(*a || *b)
    {
        ++digitnumber;

        if(*a) a++;
        else
            fark = (fark == 0 ? digitnumber : fark);
        
        
        if(*b) b++;
        else 
            fark = (fark == 0 ? digitnumber : fark);
        
    }
    
    printf("Digit Count : %d\n", digitnumber);
    printf("Fark : %d\n", fark);
    retval = (char*)malloc(sizeof(char)*(digitnumber + 1));
    while(digitnumber--)
    {
        if(digitnumber < fark)
        {
           printf(" a: %c\t b: %c\n",*(--a) , *(--b));

            continue;
        }
        else
        {
           printf(" a: %c\t b: %c\n",*(--a) , *(--b));
        }
    }

}


int main()
{
   char a[] =    "111";
   char b[] = "10101";
   
   addBinary(a,b);

    return 0;
}
