//Referans Semantiği - call by value

//Aslında pointer'lar herşeyi yapmaya yeterli. Fakat C++'ın yüksek performanslı dilin araçlarıyla uyum sağlamıyor. Bu yüzden 
// C++ da pointer semantiğinin yanısıra referans semantiği var.
//C++'da pointer kullanımı C'ye göre çok daha az

/*
Referans Semantiği
Akıllı Gösterici(Smart Pointer)

Terimler:
---------------
L value reference (son taraf referansı)
=====================================
R value reference (sağ taraf referansı)
    taşıma semantiği(move semantics)
    mükemmel gönderi(perfect forwarding)

***Referans bir nesnenin yerine geçen bir isim. & atomu ile kullanılır.

Declerator ve operator 
 int* p; -> buradaki * declerator
*ptr = &x; -> buradaki * operator (adres operatörü)

C++ dilinde önek(++x) ifadeler L value expression
            sonek(x--) ifadeler PR value expression
*/
#include <iostream>
int main(int argc, char const *argv[])
{
    //int &r;     //HATA: Referans değişkenler default initialize edilemiyorlar. Const nesneler gibi
    //const int x; //default initialize edilemez.

    int x = 10;
    int& r = x; //-> Burada r x yerine geçen bir isim. r demek x demek
    int y = 20;
    ++r;
    
    std::cout<<"x = "<<x<<"\n";
    r = y; // x = y;
    std::cout<<"x = "<<x<<"\n";

    return 0;
}
