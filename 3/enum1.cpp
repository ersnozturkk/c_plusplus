#include <assert.h>


enum Color {White, Gray, Red, Black};
enum Pos {Off, On, Hold, Standby};
//sizeof(enum Color) == sizeof(int) -> Always True

//C++'da enum türü seçilebiliyor.
//C++ dilinde aritmetik türlerden enum türlerine örtülü(impilict) tür dönüşümü yok. Fakat numaralandırma türlerinden aritmetik türlere
//otomatik dönüşüm maalesef var


//C++'a yeni bir enum kategorisi eklendi ve geleneksel enum tanımlamaları kullanımı çok tercih edilmiyor.
        //Geleneksel numaralandırma türlerinin dezavantajları
            //1-) enum Color2 {White, Gray, Red, Black}; // Aynı scope içinde aynı isim tanımlaması yapılamayacağı için syntax hatası Scope çakışması 
            //Enum'un scope u yok. Global alanda tutuluyor.

            //2-) numaralandırma türlerinden aritmetik türlere otomatik dönüşüm maalesef var.

//Modern C++ ile kapsamlandırılmış numaralandırma türlerini eklediler. enum2.cpp


int main(int argc, char const *argv[])
{
    //Color mycolor = 2;   //Syntax hatası 2 int türünde. Color türüne otomatik dönüşüm yok.
    return 0;
}

/*Önemli

18:30 Enum dezavantajları - header dosyası import etme sorunu

*/