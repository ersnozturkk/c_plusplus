#include <iostream>

//referans semantiğinde tür uyuşmazlığı her zaman sentaks hatası.
//Elemanları referans olan diziler oluşturulamaz.



//Neden referans kullanıyoruz?


void foo(int& r)
{
    r = 1000;
}

void swap(int &n1, int &n2)
{
    int temp = n1;
    n1 = n2;
    n2 = temp;
}
int main(int argc, char const *argv[])
{
 /*1   int x = 10;      //using reference 
    foo(x);
    std::cout<<"x = "<<x<<"\n";
 */


    int x = 10;      //Swap with reference
    int y = 54;

    std::cout<<"x = "<<x<<" y = "<<y<<"\n";
    swap(x,y);
    std::cout<<"x = "<<x<<" y = "<<y<<"\n";
 
    return 0;
}



/*
Referans semantiği anlama sorusu

int x = 10;                             //X'in değeri ne olur sorusuna C'de 10 cevabı verilirken
func(x);                                //C++ dilinde fonksiyonun türüne göre değişir. yani cevap fonksiyonu görmem lazım olacak.
printf("%d",x)
*/
