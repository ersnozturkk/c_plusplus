
enum class Color {White, Gray, Red, Black};
enum class Trafficlight {Green, Gray, Red, Black};
//Kapsamlandırılmış numara türleri - 
//kapsamlandırılmış numaralandırma türlerinden aritmetik türlere otomatik tür dönüşümü yok. Tür dönüşümü operatörüyle yapılabilir.
//Hoca C tarzı tür dönüşümlerini önermiyor.

//C++ da hem geleneksel enum türlerinde hemde kapsamlandırılmış enum türlerinde underline type'ı beelirtebiliyoruz.
enum class Color2:short {White, Gray, Red, Black};
enum  Color3:short {White, Gray, Red, Black};

int main(int argc, char const *argv[])
{
    //Color mycolor = Red;  //geçersiz
    Color mycolor = Color::Red; 
    Trafficlight trafficlight = Trafficlight::Red;

    //int ival =  Trafficlight::Gray; //geçersiz
    int ival2 = (int)Trafficlight::Gray;
    return 0;
}
