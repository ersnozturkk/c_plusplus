 #include <iostream>
/*deleted edilmiş fonksiyona cagrı*/


/*

*/
class Myclass {
public:

private:
    const int x;
};


class A {
    public:
        A(int);
};

class B {

    A ax;
};



 int main(int argc, char const *argv[])
 { 
     /* code */
     return 0;
 }
 


 int main(int argc, char const *argv[])
 {
     Myclass mx; //the default constructor of "Myclass" cannot be referenced -- it is a deleted functionC/C++(1790)
     /*
     Neden derleyici constructor ı deleted etti ?
        bir sınıf nesnesi oluşturulurken  sınıfın default constructor ı çağırılır.
        Biz bir default constructor yazmadığımız için derleyici default ctor ı kendisi yazar. 
        Derleyici default constructor ı kendisi yazarken sınıf içinde ki bütün değişkenleri default initialize eder.
        ve x değişkenini default init etmeye çalışır iken syntax hatası oluşur.
        İşte bu yüzden constructor delete edilir.
     */


    B x; //the default constructor of "B" cannot be referenced -- it is a deleted functionC/C++(1790)
     /*
    Neden derleyici constructor ı deleted etti ?
           B sınıfının default ctor ını derleyici yazacak.
    e derleyicinin yazdığı default ctor member ları default init edecek.
    sınıf türünden nesnelerin default init edilmesi default constructorlarını çağırmak demek 
    e bu durumda ax member ının default ctor ını çalıştırmaya çalışacak.
    fakat A class ının default constructor ı olmadığı için syntax hatası olacak ve derleyici
    B nin default ctor ını yine deleted edecek. 
     */
     return 0;
 }
 