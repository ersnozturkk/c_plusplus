#include "sentence.h"

/*
Cop assignment olmasaydı HATALAR
        move assignment operator function olmasaydı.
        a için allocate edilen alan geri verilmedi. 
*/


int main(int argc, char const *argv[])
{
    Sentence sx{"Bugün pazartesi günü"};
    sx.print();

        {
            Sentence a{"Neco ders anlatıyor."};
            a = sx; 
        } //xs in alocate edildiği yer free ediliyor.
    
    
    sx.print(); //ub - sx free edildi. Run time error (double free detected)
    //Ayrıca a sınıf değişkeninin allocate edildiği yer free edilmedi. 
    return 0;
}
