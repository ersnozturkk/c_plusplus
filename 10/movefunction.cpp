#include <iostream>

/*
NOT: Aynı şekilde atama yaparsak copy assignment ve move assignment arasında da aynı işlemler gerçekleşir.
*/
using namespace std;

class Myclass {
public:
    Myclass() = default;
    Myclass(const Myclass&)
    {
        cout << "copy ctor";
    }
    Myclass(Myclass&&)
    {
        cout << "move ctor";
    }

};


void func (const Myclass& r)
{
    Myclass x(r);       //r nin kaynağını kopyalamış olduk. copy constructor çağırılacak.
}



void func (Myclass&& r)
{
    Myclass x(move(r)); // r nin aynağını çalmış olduk. move constructor çağırılacak.
}



/*
Öyle bir fomksiyon yazki hem L value expr  ile hemde R value exp ile çağırılabilecek.
L value ile çağırıldığında kopyalama
R value ile çağırıldığında çalma yapacak. 
*/
int main(int argc, char const *argv[])
{
    Myclass m;

    func(m); //L value expr gönderdik
    cout << "\n*************************************************\n";
    func(move(m));
    cout << "\n*************************************************\n";

    return 0;
}

