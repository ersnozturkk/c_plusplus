#include <iostream>

class Myclass {
public:
    Myclass() {}
    Myclass(const Myclass& other) //copy ctor
    {
        std::cout << "copy ctor cagrıldı. Kaynak kopyalanacak.\n";
    }
    Myclass(Myclass&& other)    //move ctor
    {
        std::cout << "move ctor cagrıldı. Kaynak çalınacak.\n";
    }

};



int main(int argc, char const *argv[])
{
    Myclass x;

    Myclass y(x);
    Myclass z(Myclass{});
    return 0;
}
