

class A {
    public:
        A(int);
};

class B {

    A ax;
};



int main(int argc, char const *argv[])
{
    B bx;  //the default constructor of "B" cannot be referenced -- it is a deleted functionC/C++(1790)

    /*burda sentaks hatası vermesinin sebebi ise A sınıfının ctor ının private olması ve B sınıfının default ctor ı implicitly declared - deleted oldu. */
    return 0;
}
