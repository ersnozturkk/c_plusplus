#include <iostream>

using namespace std;

class Myclass {
    public:
        Myclass() {}
        Myclass(const Myclass& other) //copy ctor - Kopyalayan 
        {
            std::cout << "copy ctor Kaynak kopyalacanak\n";
        }      
        Myclass(Myclass&& other)        //move ctor - Çalan 
        {
            std::cout << "move ctor Kaynak çalınacak\n";

        }
};



int main(int argc, char const *argv[])
{

    Myclass x;

    Myclass y(x);
    
    Myclass z(move(x));
    return 0;
}
