#include <cstring>
#include <cstdlib>
#include <iostream>

class Sentence {
public:
    Sentence(const char* p);                //Default ctor
    Sentence(const Sentence& other);        //Copy ctor
    Sentence( Sentence&& other);            //Move ctor
    ~Sentence();                            //Destructor
    Sentence& operator=(const Sentence&);   //copy assignment
    Sentence& operator=(Sentence&&);        //move assignment
    void print()const;
    int lenght();
    void concat(const char*);
private:
    char* mp;
    size_t mlen;
};


Sentence::Sentence(const char*p) : mlen{strlen(p)}
{
    mp = static_cast<char*>(malloc(mlen+1));

    if (!mp) {
        std::cout << "bellek yetersiz\n";
        exit(EXIT_FAILURE);
    }
    std::cout << "edinilen bellek bloğunun adresi : " << static_cast<void*>(mp) << "\n";
    strcpy(mp,p);
}


Sentence::Sentence(const Sentence& other) : mlen(other.mlen)
{
    mp = static_cast<char*>(malloc(mlen+1));

    if (!mp) {
        std::cout << "bellek yetersiz\n";
        exit(EXIT_FAILURE);
    }
    std::cout << "edinilen bellek bloğunun adresi : " << static_cast<void*>(mp) << "\n";    
    strcpy(mp,other.mp);

}

Sentence::~Sentence() 
{
        //std::cout << static_cast<void*>(mp) << " adresindeki blok free ediliyor." << "\n";
        if(mp) //Kaynağını çaldığım nesnenin pointer ını null pointer yaparsak geri vermeyecek.
                free(mp);
}





Sentence& Sentence::operator=(const Sentence& other)
{
//  std::cout << static_cast<void*>(mp) << " Adresindeki blok free ediliyor.\n";
    if(&other == this)  //self assignment - aynı nesneyi kendine kopyalıyorsa.
        return *this;
    
    free(mp);
    mlen = other.mlen;

    mp = static_cast<char*>(malloc(mlen+1));

    if (!mp) {
        std::cout << "bellek yetersiz\n";
        exit(EXIT_FAILURE);
    }
    std::cout << "edinilen bellek bloğunun adresi : " << static_cast<void*>(mp) << "\n";    
    strcpy(mp,other.mp);
}

Sentence::Sentence(Sentence&& other) : mp(other.mp), mlen(other.mlen)
{
    other.mp = nullptr;
}

Sentence& Sentence::operator=(Sentence&& other) //taşıyan operator func
{
    if(&other == this)  //self assignment - aynı nesneyi kendine kopyalıyorsa.
        return *this;


    free(mp);
    mp = other.mp;
    mlen = other.mlen;

    return *this;

}






void Sentence::print()const
{
    std::cout << "(" << mp << ")" << "\n";
}

int Sentence::lenght()
{
    return mlen;
}
