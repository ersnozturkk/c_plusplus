#include <iostream>

class A {

};

void func(const A&) {
    std::cout << "func(const A &)\n";
}

void func(A&&) {
    std::cout << "func(A &&)\n";
}



int main(int argc, char const *argv[])
{
    A ax;

    func(ax); //ilk fonksiyon çağırılır.
    func(static_cast<A&&>(ax)); //2. yi çağırak için tür dönüşümü yaptık. 
    //Eğerki static_cast kullanmadn yapmak istiyorsak move kullanacağız.
    func(std::move(ax));    //aldığı ifadeyi R value ye çeviriyor.
    //Yani move doesn't move
    

    return 0;
}
