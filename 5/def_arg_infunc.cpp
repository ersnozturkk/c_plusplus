#include <iostream>

int func(int a, int b, int c = 10) // varsayılan argümanın kullanılıp kullanılmayacağı compiler time da beelirleniyor.
{
    return a + b + c;
}


int main(int argc, char const *argv[])
{
    std::cout<<func(1,2);
    return 0;
}
