
constexpr int square(int x){
    return x * x;
}

int main(int argc, char const *argv[])
{

    int x = 7;
    int a[square(15)];
    
    square(x+6);  //Artık fonksiyonun geridönüş değeri Runtime da hesaplanacak.
    
    return 0;
}
