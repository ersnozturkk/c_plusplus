int foo();

int main(int argc, char const *argv[])
{

    constexpr int x = foo();
    return 0;
}
