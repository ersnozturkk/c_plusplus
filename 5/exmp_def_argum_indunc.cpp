
#include <iostream>
#include <ctime>

void printDate(int day = -1, int mon = -1, int year = -1);



int main(int argc, char const *argv[])
{
    printDate(15,4,1988);
    printDate(15,4);
    printDate(15);
    printDate();
    
    
    return 0;
}


void printDate(int _day, int _mon, int _year){

    if(_year == -1)
    { 
        std::time_t timer;
        std::time(&timer);
        auto p = std::localtime(&timer);
        _year = p->tm_year+1900;
        if(_mon == -1){
            _mon = p->tm_mon+1;
            if(_day == -1){
                _day = p ->tm_mday;
            }

        }
    }
        
    printf("%02d-%02d-%d\n",_day,_mon,_year);
}
