#include <cstdint>

/*
function overloading olup olmadığını anlamak için fonksiyonları tanımla
Eğer syntax hatası varsa function overloading yok.
*/

typedef int itype;

using itype = int;


using fptr = int (*)(int);


void func0(int & x){}
void func0(const int & x){}

void func3(int x){}
void func3(const int x){} // FO YOK - const imza yı değiştirmez.

void func2(int *){}
void func2(const int *){}   //Function overloading - Çok sık kullanılan overloading türü

void func(int* p){}
void func(int* const p){}   //

void func4(char){} //char ın işaretli olup olmayacağı derleyiciye bağlı. fakat yine de bu 3 türde farklı türler imzalarıda farklı
void func4(signed char){}
void func4(unsigned char){}

void func5(int){}
void func5(itype){}     //redefinition of ‘void func5(itype)’gcc

void func6(int (*)(int)){}
void func6(fptr){} 


void func7(int){}
void func7(int32_t){} //FO olup olmayacağı derleyiciye bağlı. Çünkü int32_t 4 byte'lık türeş türüne eşittir. sistemten sisteme de bu değişebilir. benim derleyicim de typedef signed int int32_t
//bu yüzden de  FO yok


void func8(int &){}
void func8(int &&){}
void func8(const int &){} 
