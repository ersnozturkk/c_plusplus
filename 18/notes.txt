String Sınıfı - Üretimde en çok kullanacağımız sınıflardan biri. Mülakatlarda çok geliyor.


String sınıfı aslında bir sınıf şablonundan oluşmuştur. Sınıf şablonlarını daha sonra işleyeceğiz.

sequential containers
vector
deque
list
forward_list
array

Burada conteinar'lara atıfta bulunacağız.

String in bazı nested type ları var.

string::size_type  ==> size_t türünün eş ismi (buda unsigned int türünün eş ismi)

    Nerelerde kullanılıyor.
        a) yazı uzunluğu
        b) index türü
        c) tane/adet değeri
string::npos
