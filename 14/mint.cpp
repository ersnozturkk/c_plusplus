#include "mint.h"
#include <iostream>

int main(int argc, char const *argv[])
{
    using namespace std;
    
    Mint x{20},y{11},z{};

    cout << x << "  " << y << "  " << z << "\n";
    return 0;
}



std::ostream& operator<<(std::ostream& os, const Mint& m)
{
    return os << "(" << m.mval << ")";
}

