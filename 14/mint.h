#include <iosfwd> //iostream için sadece io bildirimlerin olduğu daha küçük boyutlu bir header

class Mint{

public:
    explicit Mint(int x = 0) : mval{ x }{}; //explicit constructor otomatik dönüşümü engeller.
    friend std::ostream& operator<<(std::ostream& , const Mint&);

private:
    int mval;
};