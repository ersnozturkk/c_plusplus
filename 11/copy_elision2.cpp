/*Derleyicinin kendi başına geçici nesne oluşturması*/




#include <iostream>

using namespace std;

class Myclass {

public:
    Myclass()
    {
    cout << "Ctor\n";
    }
    
    Myclass( int x )
    {
    cout << "Ctor (int x) x = " << x << "\n";
    }

    ~Myclass()
    {
    cout << "Destructor\n";
    }

    Myclass(const Myclass&)
    {
        cout << "Copy ctor\n";
    }

};
/*
destructor 2 kere çağırıldı.
*/
void func(Myclass x){
    cout << "Func inideyiz\n";
}

int main(int argc, char const *argv[])
{
    Myclass mx;

    func(mx); //copy constrıctor çağırılacak. 2 kere destructor çağırıldı. biri fonksiyonun parametresi için diğeri mx için

    cout << "\n---------------------------------------------------------------------------------------------------------\n";

    func(Myclass()); //Copy elision oluyor. 
    /*
    Aslında assembly kodu olarak düşünürsek func fonksiyonunda x parametre değişkeni için bellekte oluşacak yer belli olduğu için 
    derleyici koda bakarak kopyalamak yerine geçici nesneyi parametre değişkeni x in bellekte stack te ayrılan yerinde oluşturuyor.

    */
    return 0;
}
