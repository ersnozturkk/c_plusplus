
#include <iostream>

using namespace std;

class Myclass {

public:
    Myclass()
    {
    cout << "Ctor\n";
    }

    Myclass(const Myclass&)
    {
        cout << "Copy ctor\n";
    }

};

int main(int argc, char const *argv[])
{
    
    return 0;
}
