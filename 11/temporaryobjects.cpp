
#include <iostream>

using namespace std;

class Myclass {

public:
    Myclass()
    {
    cout << "Ctor\n";
    }
    
    Myclass( int x )
    {
    cout << "Ctor (int x) x = " << x << "\n";
    }

    ~Myclass()
    {
    cout << "Destructor\n";
    }

    Myclass(const Myclass&)
    {
        cout << "Copy ctor\n";
    }

};

void func(const Myclass &){
    cout << "Func inideyiz\n";
}

int main(int argc, char const *argv[])
{
    /*Geçici nesneler hangi ifade içinde kullanılıyorsa o kodun yürütülmesi sona erdiğinde nesnenin destructor ı çağırılır.*/
    cout << "\nmain Başladı\n";
    
    Myclass(10);    //Geçici nesne
    Myclass();    //Geçici nesne
    Myclass{ };    //Geçici nesne


    func(Myclass{});
    cout << "\nmain Bitti\n";

    return 0;
}
