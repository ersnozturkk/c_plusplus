
#include <iostream>

using namespace std;

class Myclass {

public:
    Myclass()
    {
    cout << "Ctor\n";
    }
    
    Myclass( int x )
    {
    cout << "Ctor (int x) x = " << x << "\n";
    }

    ~Myclass()
    {
    cout << "Destructor\n";
    }

    Myclass(const Myclass&)
    {
        cout << "Copy ctor\n";
    }

};

void func(const Myclass &){
    cout << "Func inideyiz\n";
}

int main(int argc, char const *argv[])
{
    /*Geçici nesneler hangi ifade içinde kullanılıyorsa o kodun yürütülmesi sona erdiğinde nesnenin destructor ı çağırılır.
    bir istisna dışında : o nesne bir referansa bğlandıysa.
    
    peki o nesnenin destructor ı ne zaman çağırılır:tanımlandığı skopun bitiminde
    */
    cout << "\nmain Başladı\n\n";
    
    {
        const Myclass &r = Myclass{};
    }
    Myclass &&r2 = Myclass{};

    cout << "\nmain Bitti\n\n";

    return 0;
}
