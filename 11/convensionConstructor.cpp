#include <iostream>

class Myclass{
public:
    Myclass(){
        std::cout << "default ctor this : " << this << "\n";
    }
    Myclass(int x){ //*
        std::cout << "default ctor(int) x :" << x << " this : " << this << "\n";

    }
    ~Myclass(){
        std::cout << "destructor: " << this << "\n";

    }
    Myclass& operator=(const Myclass& other) 
    {
        std::cout << "copy assigment this: " << this << " Other : " << &other << "\n";
        return *this;

    }


};

int main(int argc, char const *argv[])
{
    int ival = 10;
    Myclass m;      //Default ctor çağırıldı.

    m = ival ;  //* sayesinde syntax hatası olmuyor. bu m = Myclass{ ival } ile aynı.   //Default ctor(int) çağırıldı.  başka bi adreste geçici nesne oluşturuldu. adresler farklı bak
    /* BURADA NE OLUYOR:
        önce sınıfın int parametreli ctor kullanılarak bir geçici nesne oluşturuluyor.
        Sonra da bu geçici nesneyi m değişkenine atamak için sınıfın kopyalayan atama operator işlevini kullanıyor.
    */
   std::cout << "main devam ediyor.\n";


    return 0;
}
