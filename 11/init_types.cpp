


class Nec {
public: 
    Nec();
    explicit Nec(int);

};



int main(int argc, char const *argv[])
{
    Nec x; //Default initializing - Default constructor olmak zorunda.
    Nec y{}; // Value initializing
    Nec z(20); // Direct init.
    Nec t{37}; //Direct list initializing
    
    Nec nec = 90; //Copy initializing - sağ tarafta ki ifade int parametreli c.tor a parametre olarak gönderiliyor. - Constructor explicit ise syntax hatası
    //Yani bizim contructur ımmızın olduğunu biliyorsak Copy init ile explicit olup olmadığını anlayabiliriz.
    return 0;
}

//Sınıfların bi çoğunun tek parametreli constructorları explicittir. Bilerek isteyerek explicitli yapmak gerekmiyorsa zaten böyle yapılması gerekir. 
