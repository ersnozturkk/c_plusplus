#include <iostream>

using namespace std;

class Neco {

public:
    Neco() = default;
    explicit Neco(int x); //Bu conversion constructor tür dönüşümü yapar fakat ama explicit olarak. Yani tür dönüşümü operatörünün kullanılmasıyla yapabilir sadece.
    //implicit dönüşüm yapmaz.
    // :)    explicit uzun bir anahtar sözcük olsayddı <explicit conversion only> olurdu. :)
};



int main(int argc, char const *argv[])
{
    int ival {234};
    Neco nec;

    nec = static_cast<Neco>(ival); //explicit ile kontrollü bir dönüşüm yaptık.
    nec = ival; //explicit anahtar sözcüğü yüzünden legal değil.
    return 0;
}
