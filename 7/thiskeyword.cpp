#include <iostream>

class Ers {
public:
    void func();
};

void Ers::func()
{
   std::cout <<"this : " << this << "\n";
}



int main(int argc, char const *argv[])
{
    Ers ersin;

    std::cout <<"&ersin: " << &ersin << "\n";

    ersin.func();
     
    return 0;
}
