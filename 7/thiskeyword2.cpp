#include <iostream>

class Nec {
public:
    void func();
    void foo();
private:
    int mx;

};



void g1(Nec* p);
void g2(Nec &r);
void g3(Nec);

void Nec::func()
{
    g1(this);
    g2(*this);
    g3(*this);
}
