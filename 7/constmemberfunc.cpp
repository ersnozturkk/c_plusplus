/*
const üye işlevler içinde sınıfın non-static veri elemanlarını değiştiremeyiz.
const üye işlevler içinde sınıfın non-const üye fonksiyonlarını çağıramayız.
const sınıf nesneleri için sınıfın non-const üye fonksiyonlarını çağıramayız.+
*/

class Ers {

public:
    void func();    //Gizli parametre değişkeni:  Nec*
    void foo()const; //Gizli parametre değişkeni: const Nec*
    //void foo2()const;
private:
    int mx;
};



void Ers::func(){
    mx = 20;
}

void Ers::foo()const{    //declaration is incompatible with "void Ers::foo() const" (declared at line 11)C/C++(147)
    //mx = 20;
    //func(); //const üye işlevler içinde sınıfın non-const üye fonksiyonlarını çağıramayız.
    //foo2();
}

int main(int argc, char const *argv[])
{
    const Ers myne{};

    myne.func();
    myne.foo();

    return 0;
}


