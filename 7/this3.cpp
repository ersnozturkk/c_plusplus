#include <iostream>


class Nec {

public:
    Nec &func();
    void foo();

private:
    int mx;
};



Nec& Nec::func(){
    //...
    return *this;
}


int main(int argc, char const *argv[])
{
    Nec mynec;

    mynec.func().foo(); 
    return 0;
}
