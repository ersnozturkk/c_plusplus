//Gösterdiği nesne artık yaşamıyor.
/*
ptr = (int)malloc(n * sizeof(int));

free(ptr);
***artık ptr dangling pointer. invalid pointer

C++'da NULL pointer yaygın olarak kullanılıyor.

Null pointer var null referans yok
pointer ların dizisi olur referansların dizisi olmaz. Dizilerde referans tutulamaz


Anti pattern -> yapılmaması gerekenler
design pattern -> yapılması tavsiye edilen


//Bir fonksiyonun tek bir return statement ı olması gerekmiyor. Önerilen birden fazla gerekiyorsa kullanılması.

*/
