/*  2. KURAL SETİ
b) decltype operatörünün operandı bir isim formunda ifade değil ise
    decltype(x+5)
    decltype(*ptr)
    decltype((x))
        Bu durumda decltype karşılığı elde edilen tür parantez içindeki değerin value category'sine bağlı
            a) eğer ifade Pr value expression ise decltype yerine gelen tür T
            b) eğer ifade L value expression ise decltype yerine gelen tür T&
            b) eğer ifade X value expression ise decltype yerine gelen tür T&&
*/

#include <iostream>
int main(int argc, char const *argv[])
{
    int x = 10;
    int y = 20;
    int z = 30;
    int* ptr = &x;
    ++x = 20;
    //x++ = 30;     Syntax
    decltype(x+5) t1;       //Tür:int
    decltype(*ptr) t2 = x;  //Tür:int&     *ptr Sol taraf değeri olduğu için t2'nin türü  int&. Bu yüzden de ilk değer vermek mecburi
    decltype(x) as = y;     //Türü:int
    decltype((x)) as2 = y;  //Türü: int& --x'in parantez içinde yazılması artık ifade olduğundan türü int&
    decltype(++x) zx = z; 
    decltype(x++) zyx;      
    std::cout<<zx<<"\n";        //bir işlem kodu üretilmediği için değişmedi.
    return 0;
}


/*
NOT: Öyle durumlar varki
yazılan kodda bir ifade olmasına karşın
derleyici dilin kurallarına göre
o ifade için bir işlem kodu üretmiyor
*/