/*
Normalde C++ dilinde referansa referans olmaz;
    Ancak tür çıkarımı yapılan bağlamda referansa referans oluşabilmektedir.
    Bu durumda derleyici reference collapsing denilen kuralları uygular.

T &     &     T&
T &     &&    T&
T &&    &     T&
T &&     &&   T&&

*/

int main(int argc, char const *argv[])
{
    int x{};
    auto&& r = x;  //x sol taraf değeri olduğu için auto ya karşılık gelen tür int& oldu ve sağ taraf referansına sol taraf referansı oluştu.
    //reference collapsing kurallarına göre r nin türü int& olması gerekiyor.

    auto&& x2 = x+10;   //sadece bu durumda değişkenin türü int && (Yani sadece auto&& e sağ taraf referansı atarsak)
        //2 tane & 
    return 0;
}
