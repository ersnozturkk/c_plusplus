#include <iostream>

const int* foo();

void func(int* ptr);
void func2(int& ptr);

int main(int argc, char const *argv[])
{
     int x = 10;
     int y = 20;
     const int z = 30;
     int const& r = x;
     
     const int& r2 = y; //const u önce yada int i önce yazabiliriz. Ama tercih edilen bu kullanım.
     
     int* ptr = foo();  // invalid conversion from ‘const int*’ to ‘int*’ - const int ten int türüne otomatik dönüşüm yok.
     func(&z);  //argument of type "const int *" is incompatible with parameter of type "int *"C/C++(167) - Sysnax Hatası const int'ten int'e otomatik tür dönüşümü yok.
     func((int*)&z);  //tür dönüşümünü manuel yapmak ise undefined behivior (Tanmısız davranış)
     return 0;
}
