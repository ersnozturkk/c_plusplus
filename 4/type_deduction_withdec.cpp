#include <iostream>

int func(int){};
int func2(char* p1){};

int main(int argc, char const *argv[])
{
    const int xrt = 10;
    auto& r = xrt;  //const int &r

    int a[]{1, 2, 3, 4, 5};
    auto& x = a;    //int[5]
        int(&xy)[5] = a; //Üstte ki tanımlama ile aynı.


    auto& s = "ezgi"; //const char (&s)[5]
    const char(&sx)[5] = "ezgi";   

    auto& r4 = func; //int (&)
    int(&r2)(int) = func;     //function reference
    int(*r3)(int) = func;     //Function pointer



   std::cout<<"Hello";

    auto& val = func2;
    int(&val2)(char*) = func2;
    
    return 0;
}
