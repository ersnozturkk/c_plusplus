int main(int argc, char const *argv[])
{
    double dval = 1.2;
    int& r = dval;
    int& r2 = 10;
    /*
    Aşağıda ne oluyorda syntax hatası gitti?

        int temp_val{10};       derleyici geçici bir nesne oluşturuyor.
        const int &r = temp_val;
        Örnek: const3.cpp de
    
    */
    {
    double dval = 1.2;
    const int& r = dval;
    const int& r2 = 10;
    }
    return 0;
}

/*
Dikkat!!!
null pointer diye bir kavram var.
fakat null referans diye bir kavram yok.
*/
