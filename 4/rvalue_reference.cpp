//C++17 güncellemeleriyle eklendi
//Eklenme amacı
/*
    Move semantics(taşıma semantiği) Howard Hinnart
    perfect forwarding
*/


int main(int argc, char const *argv[])
{
    int x = 10;
    int& r = x; //sol taraf referansı
    int&& y = x+5;
    int&& y = x; //sağ taraf referansı (Error:sağ taraf referansına sol taraf referansı attın. an rvalue reference cannot be bound to an lvalueC/C++(1768))
    /*R value referanslar genelde fonksiyonlara gönderilirken kullanılıyor.*/

    return 0;
}
