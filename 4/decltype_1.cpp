//decltype auto anahtar sözcüğünden tamamen fartklı
    //Decrelation Type   
#include <iostream>

/*
a) decltype operatörünün operandı bir isim formunda ise
    decltype(x)
    decltype(ptr->x)
    decltype(a.x)
    


b) decltype operatörünün operandı bir isim formunda ifade değil ise
    decltype(x+5)
    decltype(*ptr)
    decltype((x))
        Bu durumda decltype karşılığı elde edilen tür parantez içindeki değerin value category'sine bağlı
            a) eğer ifade Pr value expression ise decltype yerine gelen tür T
            b) eğer ifade L value expression ise decltype yerine gelen tür T&
            b) eğer ifade X value expression ise decltype yerine gelen tür T&&
            
    

*/
int main(int argc, char const *argv[])
{
    int x = 56;
    int& r = x;
    int a[10]{};
    const int b[3]{1,2,3};
    const int y{12};

    decltype(x) t; //int
    decltype(r) r_dec = x; //int&. ayrıca int& olduğu için init edilmek zorunda
    decltype(a) a_dec; //int[10]
    decltype(b) b_dec = {4,5,6}; //const int[3] , const olduğu için init edilmek zorunda
    decltype(a) k;          //Syntax hatası yok dizi init edilmek zorunda değil
    decltype(a) c[20];      //int[20][10];   Bende int[10] dan 20 tane var
    return 0;
}
