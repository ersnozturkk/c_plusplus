/*
- Type Deduction - Tür Çıkarımı
- C'de yok
- Type Deduction programın çalışma zamanıyla ilgisi yoktur. Tamamen derleme zamanıyla alakalı işlemleri içermektedir.
- 

auto
decltype
decltype(auto)
template
lambda expression
*/
struct Data
{
    
};

Data foo();

int main(int argc, char const *argv[])
{
    int a[10]{};
    const int xx = 10;
    const int xy = 10;

    int& r = xy;
    const int arr[] = {1,2,3,4,5};
    auto x; //auto değişken türünde ki değişkenlere ilk değer vermek zorunludur. 

    auto y = 10;
    auto dval = 1.0;        //(double)(1.0)
    auto llval = 12LL;      //(long long)12LL
    auto sval = foo();      //Data foo()
    auto arrval = a;        //int* arrval
    auto constarrval = arr; //const int *constarrval
    auto stringliteralval = "ersin"; //const char *stringliteralval
    auto constintvar = xx;  //int constintvar
    auto refval = xy;       //int refval

    /*
    Const ve ref nesne kullanıldığında const'luk ve ref'lik düştü.
    */ 

    /*auto kullanmak sebepleri
    Daha kolay kod yazmak
    Tür hatalarını önlemek için
    
    */
    
    return 0;
}
