#include <iostream>
//type deduction (haricinde) ref to ref yok.
int main(int argc, char const *argv[])
{
    int a[5] = {1, 2 , 3 , 4 , 5};

    int(&r)[5] = a;
    int(*ptr)[5] = &a;

    r[2] = 10;
    printf("a[2] -> %d\n", a[2]);
    
    
    *(ptr[2]) = 20;
    printf("a[2] -> %d\n", a[2]);
    
    return 0;
}
