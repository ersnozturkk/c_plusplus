#include <iostream>

using namespace std;

void func(int&)
{
    cout << "int &\n";
};

void func(int&&)
{
    cout << "int &&\n";
};

void func(const int&)
{
    cout << "const int &\n";
};

void func(const int&&)
{
    cout << "const int &&\n";
};

int main(int argc, char const *argv[])
{
    int x = 10;
    const int cx = 20;

    func(x);
    func(cx);
    func(20);
    func(move(x)); //int&&
    func(move(cx)); //Dikkat void func(const int&&) fonksionu olmasa const int & 
    return 0;
}
