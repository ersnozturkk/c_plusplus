#include <iostream>

enum class Weekday {        //Ne kadar class anahtar class kelimesi olsa da enum bi class değil.
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
};

Weekday& operator++(Weekday& wd)    //prefix overload
{
     return wd = wd == Weekday::Saturday ? Weekday::Sunday : static_cast<Weekday>(static_cast<int>(wd) + 1);
}
Weekday operator++(Weekday& wd, int)   //postfix overload
{
    auto temp {wd};
    operator++(wd);
    return temp;
}

std::ostream& operator<<(std::ostream& os, const Weekday& wd)
{
    static const char* const pdays [] {
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"};

    return os << pdays[static_cast<int>(wd)];
}

int main(int argc, char const *argv[])
{
    Weekday wd{Weekday::Monday};

    ++wd;// sentaks hatası. Çünkü operator fonksiyon yok. Fonsksiyonlaı sonradan yazdım. o yüzden sentaks hatası kalkar.
    //--wd; // Operator overload fonksiyonu yok. 




    while(true)
    {
        std::cout << wd++ << " - ";
        getchar();
    }

    return 0;
}
