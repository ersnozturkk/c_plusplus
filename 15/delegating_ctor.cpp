#include <iostream>


//DEzavantajı: yalan söylüyor olması. Burada init etmiş olmuyoruz aslında. iş işten çoktan geçti. class member lar init edildi zaten.
//Member lar default olarak initailize edildi zaten.

//Moderc C++ bi alternatif sunuyor bize. Bu drumda delegating ctor u kullan.
class Myclass {
public:
    Myclass(int)
    {
        init();
    }
        
    Myclass(int, int)
    {
        init();
    }
    
    Myclass(const char*)
    {
        init();
    }
    

private:
    void init();
};


//Delegating Constructor 
class Myclass {
public:
    Myclass(int a) : Myclass(a,a,a)   //DELEGATING CONSTRUCTOR
    {    }
        
    Myclass(int x, int y) : Myclass(x,y,10)   //DELEGATING CONSTRUCTOR
    {    }

    Myclass(int x, int y,int z) : mx(x),my(y),mz(z)
    {    }
    
    Myclass(const char* p) : Myclass(std::atoi(p), 0, 0)   //DELEGATING CONSTRUCTOR
    {    }
    

private:
    int mx,my,mz;
    };


