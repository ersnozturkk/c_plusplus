#include <iostream>
#include <string>
using namespace std;


//ref qualifier

class Person {

public:
    string Name()const
    {
        return mname;
    }
    const string& Name2()const
    {
        return mname;
    }
private:
    string mname;
};



int main(int argc, char const *argv[])
{
    Person per;


    auto name1 = per.Name();  //Burada copy contrut cagiriliyor. Bir kopyalama maliyeti var
    auto name2 = per.Name2(); //Burada bir kopyalama olmayacak.

    //İleride göreceğiz. Anlamadıysan geç.

    return 0;
}
