//C++ 11 standartlarında ciddi eklemeler yapılıyor. Kökten bir değişime uğruyor. ve Modern C++ olarak adandırılmaya başlanıyor.
//C++ 17 de ciddi miktarda dili değiştirdi.
//Scope(Kapsam) -> {}

//'A' karakter sabitleri C dilinde ki türü int iken C++'da ki türü char türüdür.
///C++'da aritmetik türlerden adres türlerine, adres türlerindende aritmetik türlere otomatik tür dönüşümü yok. (Tür dönüşümü belirtilerek yapılabilir.)
//C++'da const nesneler tanımlarken  ilk değer vermezsek syntax hatası.
/*#include <iostream>
using namespace std;

// main() is where program execution begins.
int main() {
   cout << "Hello World\n"; // printf Hello World
   return 0;

  x >> 2
  x >>= 2;

 ??101010101010101100
  eğer x işaret işaretsiz tam sayı türünden ise  soldan besleme  "0" ile yapılacak.
  eğer x işaret işaretli tam sayı türünden ve pozitif tam sayı ise yine soldan besleme  "0" ile yapılacak.
  eğer x işaret işaretli tam sayı türünden ve negatif tam sayı ise yine soldan besleme  unspecified behavior olarak derleyiciye bağlı.
         1 ile de besleyebilir 0 ile de


unspecified behavior ve implementation defined

unspecified behavior'da derleyici nasıl kod ürettiğini bildirmek zorunda değil.
implementation defined olduğunda dereyici bunu bildirmek zorunda. Derleyicinin kitabında yazar. mesela char türünün işaretli mi işaretsiz mi olduğu
implementation defined.(Derleyiciye kalmış ama bunu nasıl yaptığını bildirecek)

Amaç derleyiciye bağlı kod yazmaktan kurtulmak olmalı.


name look up tan sonra name context kontrolü var. 
{
   int printf = 10;
   printf("Context Error"); // syntax hatası name lookup hatası yok context kontrolü hatası var. isim arama sonunda ilk olarak printf'i kendi scope'unda buldu.
   İsim aramasından sonra context kontrolü var (türler uygun mu ?)
}

String literalinin türü C'de char* c++'da const chat*. Bu yüzden C dilinde string literalini değiştirmek ub C++ dilinde syntax hatası
}*/

#include <iostream>

void printstatic(){

   static int staticinteger = 5;
   staticinteger--;

   std::cout << staticinteger << "\n\n";

}



int main(int argc, char const *argv[])
{
   printstatic();
   printstatic();
   printstatic();
   printstatic();
   return 0;
}
