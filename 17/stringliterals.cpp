


int main(int argc, char const *argv[])
{ 
    //String literallerini birden fazla satırda yazma yolları 2 tane
    const char *p =     "burada yazı basliyor"
                        "burada yazı devam ediyor";
    
    const char *q =     "burada yazı basliyor"
"burada yazı devam ediyor";


    //C++ 11 ile yukarıda ki düzeltmeleri yapmadan satır satır string literali tanımlayabiliriz.
    R"(
        "Bugün gel bana" dedim.
        "gelemem" dedi.
    )";
    //Çift tırnak içinde "( varsa delimeter ı değiştirip "ali( yapabiliriz.
        R"ali(
        "Bugün gel bana" dedim.
        "gelemem" dedi. ") "( 
    )ali";
    return 0;
}
