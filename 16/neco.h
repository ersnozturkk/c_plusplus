#include "date.h"
#include "mint.h"

class Ersn; //incomplate type türler üye elemanı olamaz.
class Ersn2; //incomplate type türler üye elemanı olamaz.

class Nec {

    private:
        Date dx;
        Mint mx;
        Ersn ex; //Bir sınıfın elemanı olabilmesi için, bir nesne oluşturabilmemiz için incomplate type olmamalı
        static Ersn ex; //static olduğıu için syntax hatası değil
        Ersn* pex;
};

//ÇOK ÖNEMLİ: 
// Sınıfa başka bir sınıf türünden statik veri elemanı koyacaksak gidipte bu sınıfın tanımlarını içeren başlık dosyalarını asla include etmeyin.
// Bu çok kaba bi hata olarak görünüyor.
// Yine pointer yada referans olacaksa yine başlık dosyalarını include etmeyin bunları incomplate type olarak kullanın
//ama veri elemanı static değilse pointer yada referans değilse derleyici bu dütleri complate type olarak görmesi gerekiyor.

