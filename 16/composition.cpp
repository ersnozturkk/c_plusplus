

using namespace std;


class Nec {

public:
    void func();
    void foo();
private:
    //friend class Myclass; //friend fonksiyonun yeri önemli mi ?
    void f();
    int ival;
};


class Myclass {

public:
    Nec nec;
private:
    void g()
    {
        nec.ival = 5; // Nec türünden bir üyesi var diye Nec sınıfının private bölümüne erişebilir diye birşey yok. Ancak friend kullanırsa bu mümkün.
    }

};




int main(int argc, char const *argv[])
{

    Myclass bx;

    bx.nec.func();
    bx.nec.f();  
    return 0;
}
