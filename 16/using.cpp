
/* typedef yerine yazma kolaylığı sağlıyor
typedef ()

*/

typedef int (*Fcmp)(const void*, const void *);

using FCMP = int(*)(const void*, const void *);

/**
 * FAKAT USING in DİLE EKLENMESİNİN ASIL SEBEBİ YAZMA KOLAYLIĞI DEĞİL.
 * Using anahtar sözcüğüyle yapılan tür eş isim bildiriminin dile eklenmesinin asıl nedeni
 * bu tür yapılan bildirimlerin template türüne dönüştürülebilmesidir
 * 
 */