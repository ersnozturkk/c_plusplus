#include <iostream>


class A {
    public:
        A(){
            std::cout << "1";
        }        
        A(const A &){
            std::cout << "2";
        }        
        ~A(){
            std::cout << "3";
        }

};




class B {
    public:
        B(A){
            std::cout << "4";
        }
        ~B(){
            std::cout << "5";
        }
};



int main(int argc, char const *argv[])
{
    //B b(A());  // 2 - 4 - 5 - 3 tahmin etmiştim. Hiç birşey yazmadı :) //Fonksiyon bildirimi olarak algılıyor.

    /*
    Sebebi : Most vexing parse
        scott mayers'in uydurduğu bir terim. Eğer bir C++ cümlesi hem bir değişken tanımlama hemde fonksiyon bildirimi anlamına geliyorsa 
        dilin kurallarına göre fonksiyon bildiriminin önceliği var. 
        yani burası

        Geri dönüş değeri B türünden olan ismi b olan parametres function pointer(A()) olan bir fonksiyon.
        En az Bir tanesi Küme parantesi - {} kullansaydık. moxt vexing parse olmayacak.
    */
    std::cout << "Helorrr\n";
    //yani şöyle;
    //B b2{   A()     }; 
    //Ama tekrar parantez içine alırsak bu durumdan kurtulabiliriz.
    //B b2{  ( A()  )   }; 
    return 0;
}
