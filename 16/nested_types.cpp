
/**
 * Nested Types = Type members = member type
 * 
 * Ne demiştik?
 * Sınıfların memberları nelerdi?
 *  data members, member functions, type members
 * 
 * Buralarda çözünürlük operatörünü kullanmaya dikkat et.
 * 
 */




class A {

    class Nested{

        private:
            void func();
    };

    void foo()
    {
        Nested nx;

        //nx.func();  // syntax hatası :  A sınıfının nested sınıfının private bölümüne erişimi yok.
        //Nested type'ın tanımına sahip olan sınıf(enclosing class)
    }
};

//




