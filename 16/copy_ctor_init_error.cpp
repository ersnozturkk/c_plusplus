#include <iostream>




class Member{

public:
    Member(){
        std::cout << "Member default c.tor\n";
    }
    Member(const Member&)
    {
        std::cout << "Member copy C tor\n";
    }
};


class Owner{

public: 
/*Eğer bi nedenden dolayı copy constructor ı kendimiz yazmak zorunda kalmış isek artık elemanların
hayata gelmesinden biz sorumluyuz. Yani copy costructor içinde bütün üye elemanlarını init etmez isek
copy constructor yerine default c.tor çağırılır.
*/
    Owner() = default;
    Owner(const Owner& other) /*: m(other.m) //Yorum satırını açarsak copy c.tor cagırılacak*/
    {
        /*Yani copy ctor u kendimiz yazarken dikkatli olmalıyız. ve düzgün kopyalama işlemi için 
        bütün üye elemanlarını initaliaze etmeliyiz. Diyelim ki sınıfa sonradan bi üye elemanı ekledik ve sonrasında 
        copy ctor içinde bu veri elemanının init etmedik. bu sefer diğer bütün elemanlar içinde edilmemiş olacak.*/
        std::cout << "Owner Copy C.tor\n";
    }
private:
    Member m;
};


int main(int argc, char const *argv[])
{
    Owner a;

    Owner b = a; 
    return 0;
}
