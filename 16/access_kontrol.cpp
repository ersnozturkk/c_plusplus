


class A {

    class Nested{

    };

public:
    static Nested foo();

};



int main(int argc, char const *argv[])
{
    auto x = A::foo(); //ismi net bir şekilde belirtmediğimiz için access control yapılmıyor.
    A::Nested y = A::foo();  // ama burda access controll değişken türü belli edildiği için yapılıyor. Çok ilginç
    return 0;
}







