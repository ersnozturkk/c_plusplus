
#include <iostream>

//auto return type ile karıştırma.

/*int main(int argc, char const *argv[])
{
    
    return 0;
}*/

//mesela yukarıda tanımlanan maini şu şekilde tanımlayabiliriz. Bu genelde template'ler ile kullanılıyor.
//çünkü template tanımlamaları çok uzun olabiliyor. Bunu yazmak yerine geri dönüş türünü sağda belirtmek daha okunur oluyor.
//Ama  bence geri dönüş türü açıkca belirtilmeli


auto main(int argc, char const *argv[]) -> int
{
    
    return 0;
}


